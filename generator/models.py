from dataclasses import dataclass
from typing import Optional


@dataclass
class Param:
    type: str
    name: str
    default: Optional[str]
    description: str


@dataclass
class Return:
    type: str
    description: str
