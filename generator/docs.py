import inspect

from typing import List, Type, Dict, Any, Optional
from collections import defaultdict
from dataclasses import dataclass, asdict

from .enums import Side
from .helpers import format_param
from .models import Param, Return
from .registry import TagRegistry
from .tags import (
    AbstractTag,
    NameTag,
    CategoryTag,
    ExtendsTag,
    DeprecatedTag,
    CancellableTag,
    ReadOnlyTag,
    StaticTag,
    VersionTag,
    ParamTag,
    SideTag,
    ReturnTag,
    NoteTag
)


class BaseDocs(type):
    def __new__(mcs, name, bases, attrs):
        instance = super().__new__(mcs, name, bases, attrs)
        if len(bases) == 0:
            # Basic docs class, just skip...
            return instance

        instance.registry = TagRegistry()
        for name, tag_type in attrs.items():
            if inspect.isclass(tag_type):
                assert issubclass(tag_type, AbstractTag)
                instance.registry.register(tag_type)

        return instance


@dataclass
class Docs(metaclass=BaseDocs):
    description: str

    def __init__(self, *args):
        super().__init__(args)

    @staticmethod
    def _tag_value(tag: Type[AbstractTag], values: List[AbstractTag]):
        if tag.duplicate_allowed:
            return [v.value for v in values]

        return values[0].value if values else tag.value

    @classmethod
    def build(cls, description: str, tags: List[AbstractTag]):
        grouped = defaultdict(list)
        for tag in tags:
            grouped[tag.identifier].append(tag)

        params = [description]
        for tag in cls.registry.values():
            values = grouped[tag.identifier]
            if tag.required and not values:
                raise ValueError(f"Tag '{tag.identifier}' does not exist!")

            value = cls._tag_value(tag, values)
            params.append(value)

        return cls(*params)

    def serialize(self) -> Dict[str, Any]:
        return asdict(self)  # noqa: Registered docs are data classes


DOCS: Dict[str, Docs] = dict()


def squirrel_doc(doc_type):
    """Register a docs class"""
    def _inner(class_type):
        assert inspect.isclass(class_type)

        class_type = dataclass(class_type)
        class_type.type = doc_type

        DOCS[doc_type] = class_type
        return class_type

    return _inner


@squirrel_doc('global')
class GlobalDocs(Docs):
    name: str = NameTag
    notes: List[str] = NoteTag
    version: Optional[str] = VersionTag
    deprecated: Optional[str] = DeprecatedTag
    side: Side = SideTag
    returns: Optional[Return] = ReturnTag


@squirrel_doc('const')
class ConstDocs(Docs):
    name: str = NameTag
    category: Optional[str] = CategoryTag
    version: Optional[str] = VersionTag
    deprecated: Optional[str] = DeprecatedTag
    side: Side = SideTag


@squirrel_doc('func')
class FunctionDocs(Docs):
    name: str = NameTag
    category: Optional[str] = CategoryTag
    notes: List[str] = NoteTag
    version: Optional[str] = VersionTag
    deprecated: Optional[str] = DeprecatedTag
    side: Side = SideTag
    params: List[Param] = ParamTag
    returns: Optional[Return] = ReturnTag

    @property
    def declaration(self) -> str:
        rtype = self.returns.type if self.returns else "void"
        params = ", ".join(format_param(p) for p in self.params)

        return f'{rtype} {self.name}({params})'

    def serialize(self) -> Dict[str, Any]:
        data = super().serialize()
        data.update({'declaration': self.declaration})

        return data


@squirrel_doc('event')
class EventDocs(Docs):
    name: str = NameTag
    category: Optional[str] = CategoryTag
    cancellable: bool = CancellableTag
    notes: List[str] = NoteTag
    version: Optional[str] = VersionTag
    deprecated: Optional[str] = DeprecatedTag
    side: Side = SideTag
    params: List[Param] = ParamTag

    @property
    def declaration(self) -> str:
        return ", ".join(format_param(p) for p in self.params)

    def serialize(self) -> Dict[str, Any]:
        data = super().serialize()
        data.update({'declaration': self.declaration})

        return data


@squirrel_doc('class')
class ClassDocs(Docs):
    name: str = NameTag
    static: bool = StaticTag
    extends: Optional[str] = ExtendsTag
    category: Optional[str] = CategoryTag
    notes: List[str] = NoteTag
    version: Optional[str] = VersionTag
    deprecated: Optional[str] = DeprecatedTag
    side: Side = SideTag


@squirrel_doc('constructor')
class ConstructorDocs(Docs):
    notes: List[str] = NoteTag
    version: Optional[str] = VersionTag
    deprecated: Optional[str] = DeprecatedTag
    params: List[Param] = ParamTag

    @property
    def declaration(self) -> str:
        return ", ".join(format_param(p) for p in self.params)

    def serialize(self) -> Dict[str, Any]:
        data = super().serialize()
        data.update({'declaration': self.declaration})

        return data


@squirrel_doc('property')
class PropertyDocs(Docs):
    name: str = NameTag
    read_only: bool = ReadOnlyTag
    notes: List[str] = NoteTag
    version: Optional[str] = VersionTag
    deprecated: Optional[str] = DeprecatedTag
    returns: Optional[Return] = ReturnTag


@squirrel_doc('method')
class MethodDocs(Docs):
    name: str = NameTag
    static: bool = StaticTag
    notes: List[str] = NoteTag
    version: Optional[str] = VersionTag
    deprecated: Optional[str] = DeprecatedTag
    params: List[Param] = ParamTag
    returns: Optional[Return] = ReturnTag

    @property
    def declaration(self) -> str:
        rtype = self.returns.type if self.returns else "void"
        params = ", ".join(format_param(p) for p in self.params)

        return f'{rtype} {self.name}({params})'

    def serialize(self) -> Dict[str, Any]:
        data = super().serialize()
        data.update({'declaration': self.declaration})

        return data
