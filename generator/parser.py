import re

from typing import List
from dataclasses import dataclass

from .registry import TagRegistry


@dataclass
class SquirrelDocTag:
    name: str
    description: str


class SquirrelDoc(object):
    description: str
    tags: List[SquirrelDocTag]

    def __init__(self, description, tags):
        self.description = description
        self.tags = tags

    @classmethod
    def parse(cls, registry: TagRegistry, docs: str):
        lines = cls._cleanup(docs)
        description = cls._parse_description(lines)
        tags = [registry.create(tag, description) for tag, description in cls._parse_tags(lines)]

        return cls(description, tags)

    @staticmethod
    def _cleanup(docs):
        lines = []
        prefix_pattern = re.compile(r'\s*\*?\s*(.+)?')

        for line in docs.split('\n'):
            match = prefix_pattern.search(line)
            text, = match.groups()
            lines.append(text if text else '')

        return lines

    @staticmethod
    def _parse_description(lines):
        description = ''

        for line in lines:
            if line.startswith('@'):
                break

            description += (line + '\n')

        return description.strip()

    @staticmethod
    def _parse_tags(lines):
        tag_pattern = re.compile(r'@(\w+)(.+)?')

        tags = []
        for line in lines:
            match = tag_pattern.search(line)
            if match is not None:
                tag, description = match.groups()
                tags.append([tag, description.strip() if description else ''])

            elif line and tags:
                tags[-1][1] += ' ' + line.strip()  # Append do description

        return tags
