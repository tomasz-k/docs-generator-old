from typing import List
from dataclasses import dataclass

from generator.helpers import path_from_text
from generator.scheme import BaseScheme
from generator.enums import Side
from generator.docs import Docs, ClassDocs, ConstructorDocs, PropertyDocs, MethodDocs


@dataclass
class ClassGroup:
    definition: ClassDocs
    constructors: List[ConstructorDocs]
    properties: List[PropertyDocs]
    methods: List[MethodDocs]

    def serialize(self):
        return {
            'definition': self.definition.serialize(),
            'constructors': [_.serialize() for _ in self.constructors],
            'properties': [_.serialize() for _ in self.properties],
            'methods': [_.serialize() for _ in self.methods],
        }

    @classmethod
    def build(cls, docs: List[Docs]):
        definition = docs[0]
        assert isinstance(definition, ClassDocs)

        constructors = [doc for doc in docs if isinstance(doc, ConstructorDocs)]
        properties = [doc for doc in docs if isinstance(doc, PropertyDocs)]
        methods = [doc for doc in docs if isinstance(doc, MethodDocs)]

        return cls(definition, constructors, properties, methods)


def _is_relevant_type(doc: Docs):
    return (
        isinstance(doc, ClassDocs) or
        isinstance(doc, ConstructorDocs) or
        isinstance(doc, PropertyDocs) or
        isinstance(doc, MethodDocs)
    )


def collect_classes(docs: List[Docs]):
    # Filter irrelevant types
    docs = [doc for doc in docs if _is_relevant_type(doc)]

    indexes = []
    for idx, doc in enumerate(docs):
        if isinstance(doc, ClassDocs):
            indexes.append(idx)

    # Insert last index, so last class will know how much elements it has
    indexes.append(len(docs))

    classes = []
    for idx1, idx2 in zip(indexes, indexes[1:]):
        classes.append(ClassGroup.build(docs[idx1:idx2]))

    return classes


class ClassScheme(BaseScheme):
    template = 'templates/class.md'

    @staticmethod
    def get_path(docs: ClassGroup) -> str:
        root_path = f'{docs.definition.side.value}'
        if docs.definition.side != Side.SHARED:
            root_path += '-side-classes'
        else:
            root_path += '-classes'

        sub_path = path_from_text(docs.definition.category) if docs.definition.category else "general"
        return f'{root_path}/{sub_path}/{docs.definition.name}.md'
