# DocsGenerator
Project for generating documentation in `md` format for Gothic 2 Online scripts.
Generator search through source code recursively in order to find squirrel documentation.
Documentation is uses C++ multiline comment, with special header:
```
/* squirreldoc (type)
*/
```
where `type` is a type of documentation (`func`, `event` or `class`).

# Schemes
Below you can find detailed specification about supported schemes.

## Global
`@name` name

`@side` client|server|shared

*(optional)* `@version` version

*(optional)* `@deprecated` from_version

*(optional)* `@return` (type) description

```c++
/* squirreldoc (const)
*
* Represents the client player id.
* 
* @side		client
* @name		heroId
* @return   (int)
*
*/
```

## Const
`@name` name

`@side` client|server|shared

`@category` category

*(optional)* `@version` version

*(optional)* `@deprecated` from_version

```c++
/* squirreldoc (const)
*
* Represents NPC Vob.
* 
* @category Vob
* @side		client
* @name		VOB_NPC
*
*/
```

## Function
`@name` name

`@side` client|server|shared

*(optional)* `@category` category

*(optional)* `@note` note

*(optional)* `@version` version

*(optional)* `@deprecated` since_version

*(optional)* `@param` (type) name description

*(optional)* `@return` (type) description

```c++
/* squirreldoc (func)
*
* This function will return current game resolution in pixels.
*
* @version  0.0.0
* @name     getResolution
* @side     client
* @return	({x, y}) Current resolution of the game in pixels.
*
*/
```
## Event
`@name` name

`@side` client|server|shared

*(optional)* `@cancellable`

*(optional)* `@category` category

*(optional)* `@note` note

*(optional)* `@version` version

*(optional)* `@deprecated` from_version

*(optional)* `@param` (type) name description

```c++
/* squirreldoc (event)
*
* This event is triggered every game frame, but only when user is focusing other player/npc/vob.
* 
* @version	0.0.0
* @side		client
* @name		onRenderFocus
* @note		This event is disabled by default. To enable it, use `enableEvent_RenderFocus` function.
* @param	(int) vob_type the type of currenty focused vob. For more information see {@link VobTypes}.
* @param	(int) target_id id of currently focused player/npc. If focused target is not player/npc this value is -1.
* @param	(int) x screen virtual x position.
* @param	(int) y screen virtual y position.
* @param	(string) name focused target displayed name.
*
*/
```

## Class
`@name` name

`@side` client|server|shared

*(optional)* `@category` category

*(optional)* `@note` note

*(optional)* `@version` version

*(optional)* `@deprecated` from_version

```c++
/* squirreldoc (class)
*
* This class represents ingame Vob object.
* 
* @version	0.0.0
* @side		client
* @name		Vob
* @category Vob
*
*/
```

### Constructor

*(optional)* `@note` note

*(optional)* `@version` version

*(optional)* `@deprecated` since_version

*(optional)* `@param` (type) name description

```c++
/* squirreldoc (constructor)
*
* Creates instance of Draw3d.
*
* @version  0.0.0
* @name     calculatePixelValue
* @param    (int) x Position x of element.
* @param    (int) y Position y of element.
* @param    (string) text Text that will be drawed on screen.
*
*/
```

### Property
`@name` name

*(optional)* `@readonly`

*(optional)* `@note` note

*(optional)* `@version` version

*(optional)* `@deprecated` since_version

*(optional)* `@return` (type) description

```c++
/* squirreldoc (property)
*
* Returns current maximum health of given player.
*
* @version  0.0.0
* @name     maxHealth
* @side     client
* @return   (int) Current maximum health of the player.
*
*/
```

### Method
`@name` name

*(optional)* `@static`

*(optional)* `@note` note

*(optional)* `@version` version

*(optional)* `@deprecated` since_version

*(optional)* `@param` (type) name description

*(optional)* `@return` (type) description

```c++
/* squirreldoc (method)
*
* Calculates virtual value on the screen from given pixel value.
*
* @version  0.0.0
* @static
* @name     calculatePixelValue
* @param    (int) value Virtual value
* @return   (int) Pixel value.
*
*/
```